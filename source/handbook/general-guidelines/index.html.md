---
layout: markdown_page
title: "GitLab General Guidelines"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Getting things done
1. We **use** our own software to accomplish complex collaborative tasks.
1. We take ownership and start by creating a merge request. If you see something that needs to be improved submit a merge request. Don't tell someone else about the problem and expect them to start the merge request. "If you see something don't say something, if you see something create an MR."
1. For each action or comment, it helps to ask yourself (i) does this help the company achieve its strategic goals? (ii) is this in the company's interest, and finally, (iii) how can I contribute to this effort/issue in a constructive way?
1. There is no need for **consensus**, make sure that you give people that might have good insights a chance to respond (by /cc'ing them) but make a call yourself because [consensus doesn't scale](https://twitter.com/sama/status/585950222703992833).
1. Everyone at the company cares about your **output**. Being away from the keyboard during the workday, doing private browsing or making personal phone calls is fine and encouraged.
1. Explicitly note what **next action** you propose or expect and from whom.
1. Before **replying** to a request, complete the requested task first. Otherwise, indicate when you plan to complete it in your response. In the latter case, always send a message after the task is subsequently completed.
1. If you don't have time to do something let people know when they give you the tasks instead of having it linger so they can find an alternative. You can use the text: "I have other priorities and can't help with this" or "I can complete this on May 25, please let me know if that is OK".